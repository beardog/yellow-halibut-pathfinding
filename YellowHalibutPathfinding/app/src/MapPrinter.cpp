#include "MapPrinter.h"



#include "MapInformation.h"
#include "SixCatsLogger.h"

#include <iostream>
using namespace std;

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

MapPrinter::MapPrinter() {
  //ntdh
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

MapPrinter::~MapPrinter() {
  // ntdh
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

void MapPrinter::drawPathOverMapLines(list<string>& mapLines) {

  int y = mapLines.size() -1;
  for (auto ml = mapLines.begin(); ml!= mapLines.end(); ml++) {
    for(const GamePos gp:pathInfo) {
      if (gp.y == y) {
        if ((gp.x>=0)&&(gp.x<ml->length())) {
          (*ml)[gp.x] = '*';
        }
      }
    }
    y--;
  }
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

void MapPrinter::formMapLines(list<string>& mapLines) {
  for(int y = 0; y<mapInfo->getHeight(); y++) {
    string tmps = "";
    for(int x = 0; x<mapInfo->getWidth(); x++) {
      tmps += mapInfo->getObstacleAt(x, y) ? 'O' : '_';
    }

    mapLines.push_front(tmps);
  }
}

void MapPrinter::setMap(std::shared_ptr<MapInformation> mapInfoNew) {
  mapInfo = mapInfoNew;
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

void MapPrinter::print() {
  list<string> mapLines;
  formMapLines(mapLines);
  drawPathOverMapLines(mapLines);

  cout << "This is map:" << endl;
  for(const string ml: mapLines) {
    cout << ml << endl;
  }

  cout << ":-----------" << endl;
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

void MapPrinter::setPath(std::list<GamePos> path) {
  pathInfo = path;
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .


