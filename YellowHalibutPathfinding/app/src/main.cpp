#include "ProcessCLO.h"
#include "SixCatsLogger.h"

#include "AStarPathFinder.h"
#include "GamePos.h"
#include "MapFileReader.h"
#include "MapInformation.h"
#include "MapPrinter.h"

#include <memory> // shared_ptr, unique_ptr
#include <iostream>
using namespace std;

static shared_ptr<SixCatsLogger> c6;

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

//returns exit code: 0 if no errors appeared, 1 otherwise
int processMap(const string& mapFileName) {
  shared_ptr<MapInformation> mapInfo = make_shared<MapInformation>();
  mapInfo->setLogger(c6);

  unique_ptr<MapFileReader> fileReader = make_unique<MapFileReader>(mapFileName);
  fileReader->setLogger(c6);

  if (!fileReader->load(mapInfo)) {
    return 1;
  }

  GamePos start = fileReader->getStart();
  GamePos destination = fileReader->getDestination();//
  shared_ptr<AStarPathFinder> pathFinder =
    make_shared<AStarPathFinder>(mapInfo, start, destination);
  pathFinder->setLogger(c6);

  unique_ptr<MapPrinter> printer = make_unique<MapPrinter>();
  printer->setLogger(c6);
  printer->setMap(mapInfo);

  pathFinder->findPath();
  if (pathFinder->foundNoWay()) {
    C6_W1(c6, "There is no way in this map");
  }
  else {
    list<GamePos> path = pathFinder->getPath();
    printer->setPath(path);
  }



  printer->print();


  //finally
  return 0;
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

int main(int argc, char *argv[]) {

  AppSettings appSettings = {
    .logLevel = 3,
    .mapFileName = ""
  };

  c6 = make_shared<SixCatsLogger>(SixCatsLogger::Critical);

  const ClopErrorCode clopErrorCode = processCommandLineOptions(argc, argv, appSettings);
  if (clopErrorCode!= ERR_NoError) {
    ostringstream c6_ss;
    c6_ss << "Some logical error appeared: " << (int)clopErrorCode;
    c6->c(c6_ss.str());
    return 1;
  }

  const SixCatsLogger::LogLevel llArr[6] = {
    SixCatsLogger::Critical, SixCatsLogger::Warning,  SixCatsLogger::Info,
    SixCatsLogger::Debug, SixCatsLogger::Trace, SixCatsLogger::Flood
  };

  c6->setLogLevel(llArr[appSettings.logLevel]);

  // ... actual work
  int appResult = processMap(appSettings.mapFileName);


  // cout << "log level " << appSettings.logLevel << endl;
  // cout << "map file name " << appSettings.mapFileName << endl;

  return appResult;
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
