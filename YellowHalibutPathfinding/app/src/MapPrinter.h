#pragma once

#include <list> // list
#include <memory> // shared_ptr

#include "GamePos.h"

#include "SixCatsLoggerLoggable.h"
class MapInformation;

class MapPrinter final : virtual public SixCatsLoggerLoggable {
public:
  MapPrinter();
  ~MapPrinter();

  void setMap(std::shared_ptr<MapInformation> mapInfo);
  void setPath(std::list<GamePos> path);

  void print();

private:
  void drawPathOverMapLines(std::list<std::string>& mapLines);
  void formMapLines(std::list<std::string>& mapLines);

  std::shared_ptr<MapInformation> mapInfo;
  std::list<GamePos> pathInfo;
};