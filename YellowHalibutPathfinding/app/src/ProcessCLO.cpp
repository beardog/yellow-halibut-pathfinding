#include "ProcessCLO.h"

#include "docopt.h"

#include <iostream> //cout
using namespace std;

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

static const char USAGE[] =
  R"(yhpf_app takes map file and tries to find path between two points

    Usage:
      yhpf_app [--ll=<logLevel>] (-m FILE | --mapFileName FILE)
      yhpf_app (-h | --help)
      yhpf_app --version

    Options:
      -h, --help            Show this screen.
      --version             Show version.
      --ll=<N>              Logging level (0..5, 5 is flood, default 2).
      -m FILE, --mapFileName FILE  File with map to process.
)";

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

ClopErrorCode getLogLevel(const map<string, docopt::value>& args,
                          const int defaultValue, int& result) {

  int logLevel = defaultValue;

  const auto searchLogLevel = args.find("--ll");
  if (searchLogLevel!=args.end()) {
    const docopt::value v = searchLogLevel->second;
    if (v.isLong()) {
      logLevel = v.asLong();
    }
    else {
      if (v.isString()) {
        logLevel = stoi(v.asString());
      }
      // else {
      //return ERR_BadLogLevelValue;
      //do nothing because log level is not required option
      // }
    }
  }

  if ((logLevel<0)||(logLevel>5)) {
    return ERR_IncorrectLogLevelValue;
  }
  //else
  result = logLevel;

  return ERR_NoError;
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

ClopErrorCode getMapFileName(const map<string, docopt::value>& args,
                             const string& defaultValue, string& result) {

  string mapFileName = defaultValue;

  const auto search = args.find("--mapFileName");
  if (search!=args.end()) {
    const docopt::value v = search->second;
    mapFileName = v.asString();
  }
  else {
    return ERR_BadMapFileNameValue;
  }

  result = mapFileName;

  return ERR_NoError;
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

ClopErrorCode processCommandLineOptions(int argc, char *argv[], AppSettings& appSettings) {
  map<string, docopt::value> args = docopt::docopt(USAGE,
                                                   { argv + 1, argv + argc },
                                                   true,    // show help if requested
                                                   "yhpf_app 0.0.1"); // version string

  // cout << "Arguments are:" << endl;
  // for(auto const& arg : args) {
  //   cout << arg.first << ": " << arg.second << std::endl;
  // }
  ClopErrorCode tmpec = getLogLevel(args, appSettings.logLevel, appSettings.logLevel);
  if (tmpec != ERR_NoError) {
    return tmpec;
  }

  tmpec = getMapFileName(args, appSettings.mapFileName, appSettings.mapFileName);
  if (tmpec != ERR_NoError) {
    return tmpec;
  }

  // finally
  return ERR_NoError;
}