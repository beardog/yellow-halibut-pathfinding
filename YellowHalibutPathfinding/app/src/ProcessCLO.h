#pragma once

#include <string>

struct AppSettings {
  int logLevel;
  std::string mapFileName;
};

enum ClopErrorCode {
  ERR_NoError = 0,
  ERR_BadLogLevelValue,
  ERR_IncorrectLogLevelValue,
  ERR_BadMapFileNameValue
};

ClopErrorCode processCommandLineOptions(int argc, char *argv[], AppSettings& appSettings);

