#pragma once

#include <list>
#include <memory> //shared_ptr
#include <string>

#include "GamePos.h"
#include "SixCatsLoggerLoggable.h"

class MapInformation;

class MapFileReader final : virtual public SixCatsLoggerLoggable {
public:
  MapFileReader(const std::string& mapFileName);
  ~MapFileReader();

  bool load(std::shared_ptr<MapInformation> mapInfo);

  GamePos getStart() const;
  GamePos getDestination() const;

private:
  // reads file backwards (last line appears first in the list)
  bool readFile(std::list<std::string>& content);

  int evalMapWidth(const std::list<std::string> mapLines) const;

  std::string mapFileName;

  GamePos start;
  GamePos destination;
};