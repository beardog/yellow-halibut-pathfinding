#include "MapFileReader.h"

#include "MapInformation.h"

#include "SixCatsLogger.h"

#include <fstream>  // fstream

using namespace std;

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

MapFileReader::MapFileReader(const string& mfn) {
  mapFileName = mfn;
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

MapFileReader::~MapFileReader() {
  //nothing to do
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

int MapFileReader::evalMapWidth(const std::list<string> mapLines) const {
  int maxLineLen = 0;

  for(const string ml: mapLines) {
    if (ml.length() > maxLineLen) {
      maxLineLen = ml.length();
    }
  }

  return maxLineLen;
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

GamePos MapFileReader::getDestination() const {
  return destination;
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

GamePos MapFileReader::getStart() const {
  return start;
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

bool MapFileReader::load(shared_ptr<MapInformation> mapInfo) {
  list<string> mapLines;

  if (!readFile(mapLines)) {
    return false;
  }

  if (mapInfo) {
    mapInfo->reset();
  }
  else {
    mapInfo = make_shared<MapInformation>();
    mapInfo->setLogger(c6);
  }

  mapInfo->setMapSize(evalMapWidth(mapLines), mapLines.size());

  int yPos = 0;
  for(const string ml:mapLines) {
    int xPos = 0;
    for(int si = 0; si< ml.length(); si++) {
      switch(ml.at(si)) {
      case 'x':
        mapInfo->setObstacleAt(xPos, yPos);
        break;
      case 's':
        start.x = xPos;
        start.y = yPos;
        break;
      case 'f':
        destination.x = xPos;
        destination.y = yPos;
        break;
        //there is no default case here
      }
      // if (cellCh == 'x') {

      // }
      // else if (cellCh == 's') {

      // }
      xPos++;
    }
    yPos++;
  }

  return true;
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

bool MapFileReader::readFile(list<string>& content) {
  fstream fs;

  fs.open(mapFileName.c_str(),ios::in);
  if (!fs.is_open()) {
    C6_W2(c6, "Failed to open file ", mapFileName);
    return false;
  }

  string tmps;
  while(getline(fs, tmps)) {
    content.push_front(tmps);
  }
  fs.close();



  return true;
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

