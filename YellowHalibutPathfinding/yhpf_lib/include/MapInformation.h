#pragma once

#include <memory>

#include "SixCatsLoggerLoggable.h"


class MapInformation final : virtual public SixCatsLoggerLoggable {
public:
  MapInformation();
  ~MapInformation();

  void setMapSize(const int width, const int height);

  // map 0:0 point is expected at bottom left corner, like in cocos2d-x
  void setObstacleAt(const int x, const int y);
  bool getObstacleAt(const int x, const int y);

  int getWidth() const;
  int getHeight() const;

  void reset();

protected:
  bool* obstacles;
  int width;
  int height;
};
