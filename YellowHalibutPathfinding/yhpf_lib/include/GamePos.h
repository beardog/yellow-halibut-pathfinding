#pragma once

struct GamePos {
  int x;
  int y;
};

struct PathElement {
  GamePos base;
  GamePos previous;
  int cost;
};

enum PathDirection {
  PD_UNDEFINED,       // for root node
  PD_UP,
  PD_DOWN,
  PD_LEFT,
  PD_RIGHT
};