#pragma once

#include <memory> // shared_ptr
#include <list>
#include <utility> // pair

#include "GamePos.h"
#include "SixCatsLoggerLoggable.h"

class MapInformation;
class NodesPathInfo;

class AStarPathFinder : virtual public SixCatsLoggerLoggable {
public:
  AStarPathFinder(std::shared_ptr<MapInformation> mapInfo,
                  const GamePos& start, const GamePos& destination);
  virtual ~AStarPathFinder();

  std::list<GamePos> getPath() const;

  // returns true if this is last move (either path found or all possible variants checked)
  bool makeOneStep();

  // tries to build path
  // returns true is built sucessfully, false if not
  bool findPath();

  // returns true if finder is out of variants (so there is no way to be found)
  bool foundNoWay();

protected:
  GamePos start;
  GamePos destination;

  int estimateDistance(const GamePos& node) const;

  // Returns best estimated node and removes this node from reachable list;
  GamePos fetchBestEstimatedFromReachable();

  std::list<GamePos> reachable;
  std::list<GamePos> explored;

  std::shared_ptr<NodesPathInfo> nodesPathInfo;
  std::shared_ptr<MapInformation> mapInfo;
};
