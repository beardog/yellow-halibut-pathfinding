#include "AStarPathFinder.h"
#include "MapInformation.h"
#include "NodesPathInfo.h"

#include "SixCatsLogger.h"

#include <sstream> // ostringstream

using namespace std;

string pos2str(const int x, const int y) {
  ostringstream ss;
  ss << x << ":" << y;
  return ss.str();
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

AStarPathFinder::AStarPathFinder(shared_ptr<MapInformation> inMapInfo,
                                 const GamePos& inStart,
                                 const GamePos& inDestination) {
  start = inStart;
  destination = inDestination;
  mapInfo = inMapInfo;

  nodesPathInfo = make_shared<NodesPathInfo>(mapInfo);

  reachable.push_back(start);
  nodesPathInfo->setCost(start, 0);
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

AStarPathFinder::~AStarPathFinder() {
  //
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

int AStarPathFinder::estimateDistance(const GamePos& node) const {
  int result = abs(destination.x - node.x);
  result += abs(destination.y - node.y);
  return result;
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

GamePos AStarPathFinder::fetchBestEstimatedFromReachable() {
  GamePos result = reachable.front();
  int resultCost = nodesPathInfo->getCost(result) + estimateDistance(result);
  auto resultPos = reachable.begin();

  for (auto gpi = reachable.begin(); gpi!=reachable.end(); gpi++) {
    const int cc = nodesPathInfo->getCost(*gpi) + estimateDistance(*gpi);
    C6_D4(c6, "total cost for ", pos2str(gpi->x, gpi->y), " is ", cc);
    if (cc< resultCost) {
      resultCost = cc;
      result = *gpi;
      resultPos = gpi;
    }
  }

  reachable.erase(resultPos);
  return result;
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

bool AStarPathFinder::findPath() {
  const int watchdogStop = mapInfo->getWidth()*mapInfo->getHeight();

  int watchdog = 0;
  while (!makeOneStep()) {
    watchdog++;
    if (watchdog>=watchdogStop) {
      return false;
    }
  }


  return !foundNoWay();
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

bool AStarPathFinder::foundNoWay() {
  return reachable.empty();
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

list<GamePos> AStarPathFinder::getPath() const {
  list<GamePos> path;

  path.push_front(destination);
  GamePos previousPos = nodesPathInfo->getPrevious(destination);
  C6_D4(c6, "added ", pos2str(previousPos.x, previousPos.y),
        "as previous to dest ", pos2str(destination.x, destination.y));

  bool stop = false;
  int maxLenStop = 0;
  while(!stop) {
    GamePos gp = nodesPathInfo->getPrevious(previousPos);

    path.push_front(previousPos);

    C6_D4(c6, "added ", pos2str(gp.x, gp.y),
          " as previous to ", pos2str(previousPos.x, previousPos.y));

    // log("%s: added %i:%i as previous to %i:%i", __func__, gp.x, gp.y, previousPos.x, previousPos.y);
    previousPos = gp;

    stop = ((start.x == previousPos.x) && (start.y == previousPos.y));

    maxLenStop++;
    stop = stop || (maxLenStop > 100);

  }

  path.push_front(previousPos);

  C6_D1(c6,"path is:");
  for (const GamePos gp: path) {
    C6_D2(c6, ": >>>> ", pos2str(gp.x, gp.y));
  }
  C6_D1(c6,"----------------");

  return path;
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

bool AStarPathFinder::makeOneStep() {
  if (reachable.empty()) {
    return true;
  }

  GamePos node = fetchBestEstimatedFromReachable();

  C6_D2(c6, "current node is ", pos2str(node.x, node.y));

  if ((node.x == destination.x)&&(node.y == destination.y)) {
    C6_D1(c6, "found the path");
    return true;
  }

  explored.push_back(node);

  const int nodeCost = nodesPathInfo->getCost(node);

  // get new node candidates
  const int diffCount = 4;
  const int xDiff[diffCount] = {0,0,-1,1};
  const int yDiff[diffCount] = {-1,1,0,0};

  for(int dci = 0; dci<diffCount; dci++) {
    GamePos gp;
    gp.x = node.x + xDiff[dci];
    gp.y = node.y + yDiff[dci];

    //if node is a wall, skip it
    if (mapInfo->getObstacleAt(gp.x, gp.y)) {
      continue;
    }

    // if node was already explored, skip it
    bool found  = false;
    for (const GamePos egp: explored) {
      if ((egp.x == gp.x)&&(egp.y == gp.y)) {
        found = true;
        break;
      }
    }
    if (found) {
      continue;
    }

    //if node already in reachable, skip
    found  = false;
    for (const GamePos rgp: reachable) {
      if ((rgp.x == gp.x)&&(rgp.y == gp.y)) {
        found = true;
        break;
      }
    }
    if (found) {
      continue;
    }

    reachable.push_back(gp);

    const int gpCost = nodesPathInfo->getCost(gp);

    C6_D4(c6, "processing node ", pos2str(node.x, node.y), " vs ", pos2str(gp.x, gp.y));
    C6_D4(c6, "  cost is ", nodeCost, " vs ", gpCost);

    if ((nodeCost+1)<gpCost) {
      nodesPathInfo->setPrevious(gp, node);
      nodesPathInfo->setCost(gp, nodeCost+1);
    }
  }

  return false;
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
