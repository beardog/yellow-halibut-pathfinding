#include "MapInformation.h"
#include "SixCatsLogger.h"

#include <sstream> // ostringstream in C6_Xx
#include <string>
using namespace std;
#include <cstring> //memset

MapInformation::MapInformation() {
  width = 0;
  height = 0;
  obstacles = nullptr;
}

MapInformation::~MapInformation() {
  delete obstacles;
}

int MapInformation::getHeight() const {
  return height;
}

bool MapInformation::getObstacleAt(const int x, const int y) {
  if ((x<0) || (x>=width) || (y<0) || (y>=height)) {
    return true;
  }

  return obstacles[x + y*width];
}

int MapInformation::getWidth() const {
  // C6_F2(c6, "here ", width);
  return width;
}

void MapInformation::reset() {
  width = 0;
  height = 0;
  delete obstacles;
  obstacles = nullptr;
}

void MapInformation::setMapSize(const int inWidth, const int inHeight) {
  width = inWidth;
  height = inHeight;

  delete obstacles;// if there was any old setup

  obstacles = new bool[width*height];
  memset(obstacles, false, width*height);
}

void MapInformation::setObstacleAt(const int x, const int y) {
  if ((x<0) || (x>=width) || (y<0) || (y>=height)) {
    C6_D4(c6, "Bad call for ", x,":", y);
    return;
  }

  obstacles[x + y*width] = true;
}

