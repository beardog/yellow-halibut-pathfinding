#include "NodesPathInfo.h"
#include "MapInformation.h"

using namespace std;

const int NodesPathInfo::infinityNodeCost = 10*1000;

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

NodesPathInfo::NodesPathInfo(shared_ptr<MapInformation> mapInfo) {
  //
  mapWidth = mapInfo->getWidth();
  mapHeight = mapInfo->getHeight();
  costInfo = new int[mapWidth*mapHeight];
  for(int i = 0; i<(mapWidth*mapHeight); i++) {
    costInfo[i] = infinityNodeCost;
  }

  previousInfo = new GamePos[mapWidth*mapHeight];
}

NodesPathInfo::~NodesPathInfo() {
  delete costInfo;
  delete previousInfo;
}

int NodesPathInfo::getCost(const GamePos& gp) const {
  if ((gp.x<0) || (gp.x>=mapWidth) || (gp.y<0) || (gp.y>=mapHeight)) {
    return infinityNodeCost;
  }

  return costInfo[gp.x + gp.y*mapWidth];
}

void NodesPathInfo::setCost(const GamePos& gp, const int cost) {
  if ((gp.x<0) || (gp.x>=mapWidth) || (gp.y<0) || (gp.y>=mapHeight)) {
    // log("%s: bad pos value %i:%i (compared to %i and %i)", __func__,
    // gp.x, gp.y,  mapWidth, mapHeight);
    return;
  }

  // log("%s: setting %i:%i cost to %i", __func__, gp.x, gp.y, cost);
  costInfo[gp.x + gp.y*mapWidth] = cost;
  // log("%s: done setting %i:%i cost to %i", __func__, gp.x, gp.y, cost);
}

GamePos NodesPathInfo::getPrevious(const GamePos& gp) const {
  if ((gp.x<0) || (gp.x>=mapWidth) || (gp.y<0) || (gp.y>=mapHeight)) {
    GamePos gp = {.x = -1, .y = -1};
    return gp;
  }

  return previousInfo[gp.x + gp.y*mapWidth];
}

void NodesPathInfo::setPrevious(const GamePos& gp, const GamePos& previous) {
  if ((gp.x<0) || (gp.x>=mapWidth) || (gp.y<0) || (gp.y>=mapHeight)) {
    return;
  }

  previousInfo[gp.x + gp.y*mapWidth] = previous;
}
