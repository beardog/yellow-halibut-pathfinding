#pragma once

#include <memory> // shared_ptr
// #include <list>
// #include <utility> // pair

#include "GamePos.h"

class MapInformation;

class NodesPathInfo final {
public:
  NodesPathInfo(std::shared_ptr<MapInformation> inMapInfo);
  ~NodesPathInfo();

  int getCost(const GamePos& gp) const;
  void setCost(const GamePos& gp, const int cost);

  GamePos getPrevious(const GamePos& gp) const;
  void setPrevious(const GamePos& node, const GamePos& previous);

  static const int infinityNodeCost;
private:
  int mapWidth;
  int mapHeight;

  int* costInfo;
  GamePos* previousInfo;
};