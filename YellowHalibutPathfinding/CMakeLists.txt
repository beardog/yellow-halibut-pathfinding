
cmake_minimum_required(VERSION 3.14)

project(yhpf
    VERSION 0.0.1
    DESCRIPTION "PathFinding algorithm (A*) library and demo application"
    LANGUAGES CXX)

add_subdirectory(app)
add_subdirectory(docopt)
add_subdirectory(c6_lib)
add_subdirectory(yhpf_lib)