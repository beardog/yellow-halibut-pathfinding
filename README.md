# README #

_Yellow Halibut pathfinding_ is yet another library implementing A-Star pathfinding algorithm.

Here you can find the library itself and a demo application.

The project also uses some other libraries:
* [docopt](https://github.com/docopt/docopt.cpp) to parse command line options in demo application.
* [SixCatsLogger](https://github.com/beardog-ukr/six-cats-logger) for logging/debugging

